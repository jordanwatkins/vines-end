/* global Crafty */
import level1 from './scenes/level-1';
import level2 from './scenes/level-2';

init();

function init() {
  let a = Crafty.init(null, null, document.getElementById('app-root'));


  let vw = Crafty.viewport.rect()._w;
  let vh = Crafty.viewport.rect()._h;
  let vmin = Math.min(Crafty.viewport.rect()._w, Crafty.viewport.rect()._h);
  let vunit = vmin / 100;

  level1(vw, vh, vmin, vunit);

  Crafty.enterScene('level-1', {
    vw, vh, vmin, vunit
  });

  //level2(vw, vh, vmin, vunit);

  //Crafty.enterScene('level-2', {
  //  vw, vh, vmin, vunit
  //});
}
