function Morti(vw, vh, vunit) {
  Crafty.sprite('images/morti-1.gif', { morti: [0, 0, 147, 154] });
  Crafty.sprite('images/monte.gif', { monte: [0, 0, 147, 154] });

  return Crafty.e('2D, Canvas, Image, Color, Jumper, Gravity, Collision, Tween, morti')
    .attr({ x: 1 * vunit, y: vh - 32 * vunit, w: 15 * vunit, h: 15 * vunit, rotation: 360 })
    .origin(7 * vunit, 7 * vunit)
    .gravity('Floor')

    .bind('Move', function () {
      if (this.ground && !this.onRope && !this.dropping && !this.jumping) {
        console.log('on floor');
        this.vx = 0;
        this.vy = 0;

        if (this.ground.has('tree')) {
          //Crafty.viewport.centerOn(this, 1000);
          setTimeout(() => {
            this.jump();
          }, 1400);
        }
      }

      if (this.vy === 0) { this.dropping = false; }
    })

     // jump on/off rope
    .bind('CheckJumping', function (ground) {
      //console.log('check jumping', ground, this.onRope);

      if (ground && !this.jumping) {
        // jump
        this.vx = 12 * vunit;
        this.jumping = true;
      } else if (this.onRope) {
        this.rope.detach(this);
        this.gravity();

        this.onRope = false;
        this.dropping = true;

        this.rope.detach(this);
        this.gravity();

        // dismount flip
        this.tween({ rotation: 0 }, 300);
        this.flip('X');

        //this.ground = null;
        this.jumping = false;

        // give this release velocity
        this.vx = -this.rope.vrotation * (vw / 100) / 5;
        this.vy = this.rope.vrotation * (vw / 100) / 5;

        // stop rope
        this.rope.tween({ arotation: 0, vrotation: 0 }, 300);
        this.rope.tween({ rotation: 0 }, 1000);
      }
    });
}

export default Morti;
