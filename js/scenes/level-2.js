import Morti from '../entities/morti';

function level2(vw, vh, vmin, vunit) {
  Crafty.defineScene("level-2", function({vw, vh, vmin, vunit}) {
    let background = '#000';

    Crafty.background(background);

    let morti;

    let levelStarted = false;
    let levelStarting = false;

    Crafty.audio.add('jungle-loop', 'sounds/Jungle-loop2.wav');
    if (!Crafty.audio.isPlaying('jungle-loop')) Crafty.audio.play("jungle-loop", -1);

    morti = Morti(vw, vh, vunit);

    morti.bind('Move', function () {
      if (this.ground && !this.onRope && !this.dropping && !this.jumping) {
        console.log('on floor');
        this.vx = 0;
        this.vy = 0;

        if (this.ground.has('tree')) {
          Crafty.viewport.centerOn(this, 1000);
          setTimeout(() => {
            //this.jump();
          }, 1400);
        } else if (this.x > vw / 2) {
          Crafty.viewport.centerOn(this, 1000);
            Crafty.e("2D, DOM, Text").attr({ x: vw - 3 * vunit, y: 280, w: 1000 }).text("High Five!").textColor('white').textFont({size: '3vw'});

        }
      }

      if (this.vy === 0) { this.dropping = false; }
    })

    // monte
    Crafty.e('2D, Canvas, Image, Color, Jumper, Gravity, Collision, Tween, monte')
    .attr({ x: vw - 1 * vunit, y: vh - 32 * vunit, w: 15 * vunit, h: 15 * vunit, rotation: 360 })
    .origin(7 * vunit, 7 * vunit)
    .gravity('Floor')

    // rope
    Crafty.e('2D, Canvas, Color, Collision, Motion, AngularMotion, Tween, rope')
      .attr({ x: 50 * vunit, y: -1 * vunit, w: 1 * vunit, h: vh - vh / 4, rotation: 30, jumped: false })
      .color('#176006')
      .onHit('morti', function(hit) {
        if (this._children.length < 2 && !morti.dropping && morti.jumping) {
          morti.onRope = true;
          morti.rope = this;
          this.jumping = false;

          this.attach(morti);
          morti.antigravity();

          // adjust grab point
          morti.tween({ vx: 0, vy: 0 }, 0);

          this.vrotation -= 15 * vunit - vunit * vunit;
          this.arotation -= 10 * vunit - vw / 50;
        }
      });

    Crafty.e('2D, Canvas, Color, Collision, Motion, AngularMotion, Tween, rope')
      .attr({ x: 180 * vunit, y: -1 * vunit, w: 1 * vunit, h: vh - vh / 4, rotation: 30, jumped: false })
      .color('#176006')
      .onHit('morti', function(hit) {
        if (this._children.length < 2 && !morti.dropping && morti.jumping) {
          morti.onRope = true;
          morti.rope = this;
          this.jumping = false;

          this.attach(morti);
          morti.antigravity();

          // adjust grab point
          morti.tween({ vx: 0, vy: 0 }, 0);

          this.vrotation -= 15 * vunit - vunit * vunit;
          this.arotation -= 10 * vunit - vw / 50;
        }
      });

    // click capture
    Crafty.e('2D, Canvas, Mouse')
      .attr({ x: 0, y:0, w: vw, h: vh })
      .bind('Click', function(MouseEvent){
        if (levelStarted) {
          morti.jump();
        } else if (!levelStarting && !levelStarted) {
          setTimeout(() => {
            levelStarted = true;
          }, 4000);
        }
      });

    // cloud
    Crafty.sprite('images/cloud-bored-pink.gif', { cloud: [0, 0, 385, 395] });

    Crafty.e('2D, Canvas, Image, Twoway, cloud')
      .attr({ x: 85 * vunit, y: 15 * vunit, w: 29 * vunit, h: 30 * vunit})
      .bind("EnterFrame", function(eventData) {
        this.x = this.x + 0.05 * vunit;
      });

      platforms(vw, vh, vunit);
      floors(vunit);

      levelStart(vw, vh, vunit, morti, background);

      setTimeout(() => {
        console.log('level started');
        levelStarted = true;
      }, 4000);

    });
}

function levelStart(vw, vh, vunit, morti, background) {
  let titleText = Crafty.e("2D, DOM, Text").attr({ x: 220, y: 50, w: 1000 }).text("River Rush").textColor('white').textFont({size: '3vw'});

  Crafty.viewport.clampToEntities = false;
  Crafty.viewport.scale(2);

  Crafty.viewport.pan(150, 0, 0);

  setTimeout(() => {
    //Crafty.viewport.centerOn(morti, 1000);
    Crafty.viewport.pan(-200, 100, 1000);

    Crafty.one("CameraAnimationDone", function() {
      titleText.textColor('#3181ff');

      setTimeout(() => {
        Crafty.viewport.zoom(0.5, vw / 2 , vh / 2, 1500);
      }, 400);

      //Crafty.viewport.zoom(0, 0, vh / 2, 3000)
    });
  }, 1500);
}

function platforms(vw, vh, vunit) {
  // platform 1
  Crafty.e('2D, Canvas, Color, Wall')
    .attr({ x: 0, y: vh - 30 * vunit, w: 20 * vunit, h: 80 * vunit })
    .color('brown');

  Crafty.e('2D, Canvas, Color, Floor')
    .attr({ x: 0, y: vh - 29 * vunit, w: 20 * vunit, h: 1 * vunit })
    //.color('#176006');

  // platform 2
  Crafty.sprite('images/palm-1.gif', { palm: [0, 0, 85, 295] });

  Crafty.e('2D, Canvas, Image, Wall, palm')
    .attr({ x: 120 * vunit, y: Crafty.viewport.rect()._h - 30 * vunit, w: 20 * vunit, h: 80 * vunit })
    //.image('images/palm-1.gif');

  Crafty.e('2D, Canvas, Color, Floor, tree')
    .attr({ x: 126 * vunit, y: Crafty.viewport.rect()._h - 28 * vunit, w: 10 * vunit, h: 1 * vunit })
    //.color('#176006');
}

function floors(vunit) {
  Crafty.sprite('images/ground-plants.gif', { plants: [0, 0, 162, 49] });

  // water
  Crafty.e('2D, Canvas, Color')
    .attr({ x: 20 * vunit, y: Crafty.viewport.rect()._h - 3 * vunit, w: 400 * vunit, h: 223 * vunit })
    .color('blue');

    //floor 1
    Crafty.e('2D, Canvas, Color, Floor')
      .attr({ x: 0, y: Crafty.viewport.rect()._h - 3 * vunit, w: 40 * vunit, h: 223 * vunit })
      .color('#176006');

    // floor 2
    Crafty.e('2D, Canvas, Color, Floor')
      .attr({ x: 129 * vunit, y: Crafty.viewport.rect()._h - 3 * vunit, w: 10.9 * vunit, h: 223 * vunit })
      .color('#176006');

    //floor 3
    Crafty.e('2D, Canvas, Color, Floor')
    .attr({ x: 180 * vunit, y: Crafty.viewport.rect()._h - 3 * vunit, w: 400 * vunit, h: 223 * vunit })
    .color('#176006');

  /*Crafty.e('2D, Canvas, Color, Floor')
    .attr({ x: 127 * vunit, y: Crafty.viewport.rect()._h - 3 * vunit, w: 500 * vunit, h: 223 * vunit })
    .color('#176006');*/


  for (let i = 0; i < 4; i++) {
    Crafty.e('2D, Canvas, Image, plants')
    .attr({ x: 10 * vunit * i, y: Crafty.viewport.rect()._h - 7.5 * vunit, w: 10 * vunit, h: 5.5 * vunit })
    .image('images/ground-plants.gif', 'repeat-x');
  }

  for (let i = 13; i < 14; i++) {
    Crafty.e('2D, Canvas, Image, plants')
    .attr({ x: 10 * vunit * i, y: Crafty.viewport.rect()._h - 7.5 * vunit, w: 10 * vunit, h: 5.5 * vunit })
    .image('images/ground-plants.gif', 'repeat-x');
  }

  for (let i = 17; i < 44; i++) {
    Crafty.e('2D, Canvas, Image, plants')
    .attr({ x: 180 * vunit * i, y: Crafty.viewport.rect()._h - 7.5 * vunit, w: 10 * vunit, h: 5.5 * vunit })
    .image('images/ground-plants.gif', 'repeat-x');
  }
}

export default level2;
