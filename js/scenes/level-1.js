import Morti from '../entities/morti';

import level2 from '../scenes/level-2';


function level1(vw, vh, vmin, vunit) {
  Crafty.defineScene("level-1", function({vw, vh, vmin, vunit}) {
    let background = '#3181ff';

    Crafty.background(background);

    let morti;

    let levelStarted = false;
    let levelStarting = false;

    Crafty.audio.add('jungle-loop', 'sounds/Jungle-loop2.wav');
    if (!Crafty.audio.isPlaying('jungle-loop')) Crafty.audio.play("jungle-loop", -1);

    morti = Morti(vw, vh, vunit);

    morti.bind('Move', function () {
      if (this.ground && !this.onRope && !this.dropping && !this.jumping) {
        console.log('on floor');
        this.vx = 0;
        this.vy = 0;

        if (this.ground.has('tree')) {
          //Crafty.viewport.centerOn(this, 1000);
          setTimeout(() => {
            this.jump();
          }, 1400);

          setTimeout(() => {
            level2(vw, vh, vmin, vunit);

            Crafty.enterScene('level-2', {
              vw, vh, vmin, vunit
            });
          }, 7400);

        }
      }
    });

    //Crafty.enterScene('level1');

    // rope
    let rope = Crafty.e('2D, Canvas, Color, Collision, Motion, AngularMotion, Tween, rope')
      .attr({ x: 50 * vunit, y: -1 * vunit, w: 1 * vunit, h: vh - vh / 4, rotation: 30, jumped: false })
      .color('#176006')
      .onHit('morti', function(hit) {
        console.log('rope hit', this.jumped);
        if (this._children.length < 2 && !morti.dropping && morti.jumping) {
          console.log('rope grab');
          morti.onRope = true;
          morti.rope = rope;
          this.jumping = false;

          this.attach(morti);
          morti.antigravity();

          // adjust grab point
          morti.tween({ vx: 0, vy: 0 }, 0);

          this.vrotation -= 15 * vunit - vunit * vunit;
          this.arotation -= 10 * vunit - vw / 50;
        }
      });

    // click capture
    Crafty.e('2D, Canvas, Mouse')
      .attr({ x: 0, y:0, w: vw, h: vh })
      .bind('Click', function(MouseEvent){
        console.log('click');
        console.log(levelStarted, levelStarting);
        if (levelStarted) {
          morti.jump();
        } else if (!levelStarting && !levelStarted) {
          console.log('level starting');
          //levelStarting = true;
          //levelStart();

          setTimeout(() => {
            console.log('level started');
            levelStarted = true;
          }, 4000);
        }
      });

    // cloud
    Crafty.sprite('images/cloud-bored-pink.gif', { cloud: [0, 0, 385, 395] });

    Crafty.e('2D, Canvas, Image, Twoway, cloud')
      .attr({ x: 85 * vunit, y: 15 * vunit, w: 29 * vunit, h: 30 * vunit})
      .bind("EnterFrame", function(eventData) {
        this.x = this.x + 0.05 * vunit;
      });

      platforms(vw, vh, vunit);
      floors(vunit);

      //Crafty.viewport.scale(2);
      //Crafty.viewport.scroll('y', 300);

      levelStart(vw, vh, vunit, morti, background);

      setTimeout(() => {
        console.log('level started');
        levelStarted = true;
      }, 4000);

    });


}

function levelStart(vw, vh, vunit, morti, background) {
  let titleText = Crafty.e("2D, DOM, Text").attr({ x: 220, y: 50, w: 1000 }).text("Mortimer: At Vine's End").textColor('white').textFont({size: '3vw'});

  Crafty.viewport.clampToEntities = false;
  Crafty.viewport.scale(2);

  Crafty.viewport.pan(150, 0, 0);

  setTimeout(() => {
    //Crafty.viewport.centerOn(morti, 1000);
    Crafty.viewport.pan(-200, 100, 1000);

    Crafty.one("CameraAnimationDone", function() {
      titleText.textColor('#3181ff');

      setTimeout(() => {
        Crafty.viewport.zoom(0.5, vw / 2 , vh / 2, 1500);
      }, 400);

      //Crafty.viewport.zoom(0, 0, vh / 2, 3000)
    });
  }, 1500);
}

function platforms(vw, vh, vunit) {
  console.log('platforms');
  // platform 1
  Crafty.e('2D, Canvas, Color, Wall')
    .attr({ x: 0, y: vh - 30 * vunit, w: 20 * vunit, h: 80 * vunit })
    .color('brown');

  Crafty.e('2D, Canvas, Color, Floor')
    .attr({ x: 0, y: vh - 29 * vunit, w: 20 * vunit, h: 1 * vunit })
    //.color('#176006');

  // platform 2
  Crafty.sprite('images/palm-1.gif', { palm: [0, 0, 85, 295] });

  Crafty.e('2D, Canvas, Image, Wall, palm')
    .attr({ x: 120 * vunit, y: Crafty.viewport.rect()._h - 30 * vunit, w: 20 * vunit, h: 80 * vunit })
    .image('images/palm-1.gif');

  Crafty.e('2D, Canvas, Color, Floor, tree')
    .attr({ x: 126 * vunit, y: Crafty.viewport.rect()._h - 28 * vunit, w: 10 * vunit, h: 1 * vunit })
    //.color('#176006');
}

function floors(vunit) {
  Crafty.e('2D, Canvas, Color, Floor')
    .attr({ x: 0, y: Crafty.viewport.rect()._h - 3 * vunit, w: 40 * vunit, h: 223 * vunit })
    .color('#176006');

  Crafty.e('2D, Canvas, Color, Floor')
    .attr({ x: 127 * vunit, y: Crafty.viewport.rect()._h - 3 * vunit, w: 500 * vunit, h: 223 * vunit })
    .color('#176006');

  Crafty.sprite('images/ground-plants.gif', { plants: [0, 0, 162, 49] });

  for (let i = 0; i < 4; i++) {
    Crafty.e('2D, Canvas, Image, plants')
    .attr({ x: 10 * vunit * i, y: Crafty.viewport.rect()._h - 7.5 * vunit, w: 10 * vunit, h: 5.5 * vunit })
    .image('images/ground-plants.gif', 'repeat-x');
  }

  for (let i = 13; i < 100; i++) {
    Crafty.e('2D, Canvas, Image, plants')
    .attr({ x: 10 * vunit * i, y: Crafty.viewport.rect()._h - 7.5 * vunit, w: 10 * vunit, h: 5.5 * vunit })
    .image('images/ground-plants.gif', 'repeat-x');
  }
}

export default level1;
